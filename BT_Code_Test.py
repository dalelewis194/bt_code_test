"""
Author: Dale Lewis

Date Created: 23/11/2016

Description:
A script that takes in a CSV file and
checks to see if a router can be patched
depending on a criteria.

"""


# Imported for file handling
import sys
# Imported for Data Validation
import re


# Function : This function is used for reading the file
def read_file():
    try:
        # Skip the first line containing headers
        f.readline()
        # Read the rest of the file
        try:
            # Read each line of the file and put them into a list.
            data = [line for line in f.readlines()]
            # Send the file lines to be analysed
            analyse_data(data)
        except MemoryError as me:
            print("File is too large to be parsed.\n" + str(me))
    except EOFError as eof:
        print("File does not contain any data to be parsed.\n" + str(eof))


# Function : This function sends each line to find the routers needed.
# Parameter : data (List of lines in the file.)
def analyse_data(data):
    try:
        # Create lists of ip addresses and host names for validation
        ip_addresses = [i.split(",")[1] for i in data]
        host_names = [i.split(",")[0].lower() for i in data]
    except IndexError as IE:
        # If an error occurs gathering the lists then there is a problem with the file
        print("Index Error: File does not contain correct amount of columns\n" + str(IE))
        # Exit the script as the file is corrupt
        sys.exit()

    # This is a comprehension that will send each line to be checked
    # and only valid routers will be contained within checked rows.
    checked_rows = [i for i in data if(router_check(i.split(","),
                                                    ip_addresses,
                                                    host_names, ) is True)]
    output(checked_rows)


# Function : Validate and check against the rules provided
# Parameters: row (a line of the file)
#             ip_addresses (a list of all ip_addresses)
#             host_name    (a list of all host names)
def router_check(row, ip_addresses, host_names):
    # Check is the router has been patched.
    if row[2].lower() == "no":
        # Check the format of OS version
        if re.match(r"(\d+)\.?(\d+)", row[3]):
            # Check if the OS version is 12 or above
            if float(row[3]) > 12:
                # Check if the IP address appears two or more times in the file
                if ip_addresses.count(row[1]) < 2:
                    # Check if the host name appears two or more times.
                    if host_names.count(row[0].lower()) < 2:
                        return True
        else:
            # If the OS version is not the correct format then show an error and move on.
            print("The OS version: [{os}] is not a valid number.".format(os=row[3]))
    return False


# Function : This outputs the routers that need to patched in the format requested.
# Parameters : results (The routers that need to be patched)
def output(results):
    for i in results:
        result = i.split(",")
        print('{host} ({ip}), OS Version {os_version}'.format(host=result[0], ip=result[1],
                                                              os_version=result[3])),
        if len(result[4].strip()) > 1:
            print('[{notes}]'.format(notes=result[4].strip("\n")))


if __name__ == '__main__':
    try:
        # Check that the parameter given has a .csv extension
        if sys.argv[1].lower().endswith(".csv"):
            # Open the file to be processed
            f = open(sys.argv[1], 'r')
            read_file()
        else:
            print("Incorrect file extension .csv expected")
    except Exception as e:
        print("Index Error: File not found, check parameters")
        print("Error Message: " + str(e))
    finally:
        try:
            # Close the file is it was opened
            f.close()
        except NameError:
            print("File wasn't opened.")

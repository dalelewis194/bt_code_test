Bt Code Test:

Author: Dale Lewis
Date Created: 23/11/2016

Language:
Python 2.7.6

Description:
A python script that reads a CSV file and checks the routers against a set of rules to find which one needs patchwork.

Run:
Must be run in a terminal opened within the directory that contains the script and the input file.
python BT_Code_Test.py [yourfilehere].csv

Example:
python BT_Code_Test.py sample.csv

Notes:
If the file does not end with .csv the script will make you aware.

Copyright:
Dale Lewis